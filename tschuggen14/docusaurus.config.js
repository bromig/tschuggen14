// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Tschuggen 14',
  tagline: 'A modern chalet in the beautiful swiss alps',
  url: 'https://pages.gitlab.io',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'bromig', // Usually your GitHub org/user name.
  projectName: 'tschuggen14', // Usually your repo name.

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl: 'https://github.com/facebook/docusaurus/tree/main/packages/create-docusaurus/templates/shared/',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://github.com/facebook/docusaurus/tree/main/packages/create-docusaurus/templates/shared/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      navbar: {
        title: 'My Site',
        logo: {
          alt: 'My Site Logo',
          src: 'img/logo.svg',
        },
        items: [
          {
            type: 'doc',
            docId: 'intro',
            position: 'left',
            label: 'Tutorial',
          },
          {to: '/blog', label: 'Blog', position: 'left'},
          {
            href: 'https://github.com/facebook/docusaurus',
            label: 'GitHub',
            position: 'right',
          },
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'The ski resort',
            items: [
              {
                label: 'Belalp Bahnen AG',
                to: 'https://www.belalp.ch/',
              },
              {
                label: 'Weather - Bergfex Belalp',
                to: 'https://www.bergfex.ch/belalp/wetter/',
              },
              {
                label: 'Pro Natura Zentrum Aletsch',
                to: 'https://www.pronatura-aletsch.ch',
              },
              {
                label: 'Tutorial',
                to: '/docs/intro',
              },
            ],
          },
          {
            title: 'Rent now!',
            items: [
              {
                label: 'Link zu Fabiennes Seite',
                href: 'https://stackoverflow.com/questions/tagged/docusaurus',
              },
              {
                label: 'Weiterer Link',
                href: 'https://discordapp.com/invite/docusaurus',
              },
              {
                label: 'Twitter',
                href: 'https://twitter.com/docusaurus',
              },
            ],
          },
          {
            title: 'More',
            items: [
              {
                label: 'Impressum',
                to: '/blog',
              },
              {
                label: 'GitHub',
                href: 'https://github.com/facebook/docusaurus',
              },
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()} Lukas Bromig. Built with Docusaurus.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
      metadata: [
      {name: 'keywords', content: 'chalet, rent, belalp, blatten, skiing, switzerland, rental, cabin, holiday home'},
      {name: 'author', content: 'Lukas Bromig'},
      {name: 'copyright', content: 'Lukas Bromig'},
      {name: "robots", content: "index"},
      {name: "robots", content: "follow"},
      ],
      // This would become <meta name="keywords" content="cooking, blog"> in the generated HTML
    }),
};

module.exports = config;
