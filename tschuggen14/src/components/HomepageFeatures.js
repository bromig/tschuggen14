import React from 'react';
import clsx from 'clsx';
import styles from './HomepageFeatures.module.css';

const FeatureList = [
  {
    title: 'Wunderschöner Tiefschnee',
    Svg: require('../../static/img/undraw_docusaurus_mountain.svg').default,
    Img: require('../../static/img/lukas_portrait.jpg').default,
    ImgAlt:'Test text',
    description: (
      <>
        Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
        tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero
        eos et accusam et justo duo dolores et ea.
      </>
    ),
  },
  {
    title: 'Hier könnte Ihre Werbung stehen!',
    Svg: require('../../static/img/undraw_docusaurus_tree.svg').default,
    Img: require('../../static/img/lukas_portrait.jpg').default,
    ImgAlt:'Test text',
    description: (
      <>
        Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
        tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero
        eos et accusam et justo duo dolores et ea. <code>docs</code>
      </>
    ),
  },
  {
    title: 'Nix wie los!',
    Svg: require('../../static/img/undraw_docusaurus_react.svg').default,
    Img: require('../../static/img/lukas_portrait.jpg').default,
    ImgAlt:'Test text',
    description: (
      <>
        Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
        tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero
        eos et accusam et justo duo dolores et ea.
      </>
    ),
  },
];

//function Feature({Svg, title, description}) {
function Feature({Svg, Img, ImgAlt, title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} alt={title} />
        <img  src={Img} alt={ImgAlt}/>
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
